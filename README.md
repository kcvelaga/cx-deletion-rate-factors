# Variables Affecting Deletion Rate of Articles
## Created through the Content Translation tool

Task: [T356765](https://phabricator.wikimedia.org/T356765)

The [Content Translation](https://www.mediawiki.org/wiki/Content_translation) tool (CX) allows editors to create translations right next to the original article and automates the boring steps such as copying text across browser tabs, looking for corresponding links and categories, etc. The tool has been live on Wikimedia projects for over a decade, and was used to create more than 2 million articles across various language Wikipedias. As it is with every article creation process (created through CX or otherwise), articles get deleted due to various factors, such poor quality of the content, plagiarism, notability, among others. Compared to articles created without using CX, [deletion rate](https://www.mediawiki.org/wiki/Content_translation/Deletion_statistics_comparison) of the articles created using CX is significantly lower. On average, deletion rate of articles1 created using CX is ~3%, while that of non-CX article is ~12%.

The quarterly CX deletion stats comparision is used by the Language team to adjust machine translation limits on various Wikipedias to enforce review and modification of initial machine translation before articles are published. The team is working on making [improvements to the limit system](https://phabricator.wikimedia.org/T251887), and the primary goal of this analysis is to inform that work. The analysis will explore the importance and the impact of various factors in influencing the deletion outcome of articles translated through the content translation tool.

**Report at: https://kcvelaga.quarto.pub/cx-deletion-rate-variables-2024/**



